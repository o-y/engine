<?php
/** [LGPL] Copyright 2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if (Engine::isDirectRequest(__FILE__)) die ('No direct access allowed.');

require_once('classhelper.php');
require_once('error.php');

class Engine {
	
	private static $engineDir = null;
	private static $siteDir = null;
	private static $pubDir = null;
	
	private static $handlerDir = null;
	private static $viewDir = null;
	private static $libDir = null;
	private static $dbDir = null;
	
	private static $baseURL = null;
	private static $indexURL = null;
	private static $pubURL = null;
	
	private static $request = null;
	private static $segments = null;
	
	private function __construct() {}
	
	public static function setEngineDir($dir) {
		self::$engineDir = $dir;
		self::fixDirectorySyntax(self::$engineDir);
	}
	
	public static function engineDir($suffix = '') {
		return self::$engineDir . $suffix;
	}
	
	public static function setSiteDir($dir) {
		self::$siteDir = $dir;
		self::fixDirectorySyntax(self::$siteDir);
	}
	
	public static function siteDir($suffix = '') {
		return self::$siteDir . $suffix;
	}
	
	public static function setPubDir($dir) {
		self::$pubDir = $dir;
		self::fixDirectorySyntax(self::$pubDir);
	}
	
	public static function pubDir($suffix = '') {
		return self::$pubDir . $suffix;
	}
	
	public static function setHandlerDir($dir) {
		self::$handlerDir = $dir;
		self::fixDirectorySyntax(self::$handlerDir);
	}
	
	public static function handlerDir($suffix = '') {
		return self::$handlerDir . $suffix;
	}
	
	public static function setViewDir($dir) {
		self::$viewDir = $dir;
		self::fixDirectorySyntax(self::$viewDir);
	}
	
	public static function viewDir($suffix = '') {
		return self::$viewDir . $suffix;
	}
	
	public static function setLibDir($dir) {
		self::$libDir = $dir;
		self::fixDirectorySyntax(self::$libDir);
	}
	
	public static function libDir($suffix = '') {
		return self::$libDir . $suffix;
	}
	
	public static function setDBDir($dir) {
		self::$dbDir = $dir;
		self::fixDirectorySyntax(self::$dbDir);
	}
	
	public static function dbDir($suffix = '') {
		return self::$dbDir . $suffix;
	}
	
	
	public static function setBaseURL($url) {
		self::$baseURL = $url;
		self::fixURLSuffix(self::$baseURL);
	}
	
	public static function baseURL($suffix = '') {
		return self::$baseURL . $suffix;
	}
	
	public static function setIndexURL($url) {
		self::$indexURL = $url;
		self::fixURLSuffix(self::$indexURL);
	}
	
	public static function indexURL($suffix = '') {
		return self::$indexURL . $suffix;
	}
	
	public static function setPubURL($url) {
		self::$pubURL = $url;
		self::fixURLSuffix(self::$pubURL);
	}
	
	public static function pubURL($suffix = '') {
		return self::$pubURL . $suffix;
	}
	

	
	public static function getRequest() {
		return self::$request;
	}
	
	public static function getSegment($which) {
		if (count(self::$segments) > 0) {
			if (isset(self::$segments[$which])) return self::$segments[$which];
		}
		return null;
	}
	
	public static function getSegments() {
		return self::$segments;
	}
	
	public static function setup() {
		
		if (self::$engineDir == null) {
			self::setEngineDir(dirname(__FILE__).'/');
		}
		
		if (self::$siteDir == null) {
			self::setSiteDir(dirname($_SERVER['SCRIPT_FILENAME']) . '/site/');
		}
		
		if (self::$pubDir == null) {
			self::setPubDir(dirname($_SERVER['SCRIPT_FILENAME']) . '/pub/');
		}
		
		if (self::$handlerDir == null) {
			self::setHandlerDir(self::siteDir('handlers/'));
		}
		
		if (self::$viewDir == null) {
			self::setViewDir(self::siteDir('views/'));
		}
		
		if (self::$libDir == null) {
			self::setLibDir(self::siteDir('libs/'));
		}
		
		if (self::$dbDir == null) {
			self::setDBDir(self::siteDir('dbs/'));
		}
		
		if (self::$baseURL == null) {
			throw new Exception('For Engine to work properly, "baseURL" must be set');
		}
		
		if (self::$indexURL == null) {
			self::setIndexURL(self::baseURL('index.php/'));
		}
		
		if (self::$pubURL == null) {
			self::setPubURL(self::baseURL('pub/'));
		}
		
		
		self::$request = self::parsePathInfo();
		if (self::$request !== null) self::$segments = explode('/', self::$request);
	
		// remove last empty request array element (for example 'index.php/asd/' becomes 'index.php/asd')
		if (count(self::$segments) > 0) {
			if (end(self::$segments) === '') array_pop(self::$segments);
			reset(self::$segments);
		}
	}
	
	private static function parsePathInfo() {
		if (isset($_SERVER['PATH_INFO']) && strlen($_SERVER['PATH_INFO']) > 0) {
			return substr($_SERVER['PATH_INFO'], 1);
		} else {
			return null;
		}
	}
	
	/**
	 * Replace given string's '\' with '/' and makes sure it ends in '/'
	 * 
	 * @param string $dir - Filesystem path to a directory, modified by-reference
	 */
	public static function fixDirectorySyntax(&$dir) {
		$dir = str_replace('\\', '/', $dir);
		if ($dir[strlen($dir)-1] !== '/') $dir .= '/';
	}
	
	/**
	 * Adds '/' to end of an URL if it's not there already.
	 * 
	 * @param string $url - URL to be fixed, modified by-reference
	 */
	public static function fixURLSuffix(&$url) {
		if (substr($url, -1) !== '/') $url = $url.'/';
	}
	
	/**
	 * Check if HTTP client requested a file directly
	 * 
	 * @param string - $currentFile - Path to file being checked for direct access
	 * @return bool
	 */
	public static function isDirectRequest($currentFile) {
		if (realpath($_SERVER['DOCUMENT_ROOT'] . $_SERVER['SCRIPT_NAME']) === realpath($currentFile)) return true;
		else return false;
		
		/*$fullPath = str_replace('\\', '/', $currentFile);
		$fullRequestPath = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI']);
			
		if (strpos($fullRequestPath, $fullPath) === 0) return true;
		else return false;*/
	}
	
}
