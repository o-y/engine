<?php
namespace Engine;

class ClassHelper {
	/**
	 * Include a php file, instantiate a class defined there and call it's method with
	 * specified arguments.
	 * 
	 * @param string $phpFile - Path to php file where the class is defined
	 * @param string $className - Name of the class to instantiate with default constructor
	 * @param string $methodName - Name of the method to call
	 * @param array $params - Array of variables passed as arguments to the function call
	 */
	public static function invokeMethod($phpFile, $className, $methodName, $params) {
		require_once($phpFile);
		$instance = new $className();
		$callback = array($instance, $methodName);
		call_user_func_array($callback, $params);
		
		// The code below is commented because although it makes the error output
		// of a failed 'call_user_func_array()' call more clear, it is not so in
		// every case. And in that case the real reason would be hidden, as
		// no exception would be thrown-- the real reason could lie in the exact
		// message and stack trace of that exception.
		 
		/*
		if (is_callable($callback)) {
			call_user_func_array($callback, $params);
		}
		else {
			if (ini_get('error_reporting') & E_NOTICE == E_NOTICE) {
				throw new Exception(sprintf(
					'Could not invoke method "%s" on an instance of class "%s"',
					$methodName, $className, get_class($instance)
					));
			}
			else {
				Engine::error("Failed to call correct method.", 500);
			}
		}
		*/
	} // method
	
}
