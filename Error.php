<?php
namespace Engine;

class Error {

	/**
	 * Display HTTP error message
	 * 
	 * @param string - Error message to be displayed to the HTTP client (can be null)
	 * @param int|string - Error code to be sent to the HTTP client
	 * @param string - Header message to be sent to the HTTP client (can be null,
	 * in which case if header_code was standard HTTP error code, it's message will be used)
	 */
	public static function send($body=null, $header_code=404, $header_msg=null) {
		// list from http://framework.zend.com/svn/framework/laboratory/Zend_Service_SecondLife/library/Zend/Http/Response.php
		$header_messages = array(
			// Informational 1xx
			100 => 'Continue',
			101 => 'Switching Protocols',
	
			// Success 2xx
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
	
			// Redirection 3xx
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found',  // 1.1
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			// 306 is deprecated but reserved
			307 => 'Temporary Redirect',
	
			// Client Error 4xx
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
	
			// Server Error 5xx
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported',
			509 => 'Bandwidth Limit Exceeded'
		);

		$http_proto = $_SERVER['SERVER_PROTOCOL'];
		if ($http_proto === null) $http_proto = 'HTTP/1.1';

		if ($header_msg === null) {
			$header_msg = @$header_messages[$header_code];
			if ($header_msg === null) $header_msg = 'Unknown message';
		}
		
		if ($body === null) {
			$body = $header_msg;
		}

		header("{$http_proto} {$header_code} {$header_msg}");
		die($body);
	}
}
