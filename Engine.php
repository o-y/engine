<?php
/** [LGPL] Copyright 2012 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Engine;

require_once('Misc.php');
require_once('DirType.php');
require_once('URLType.php');
require_once('ClassHelper.php');
require_once('Error.php');
require_once('Versioning.php');

if (Misc::isDirectRequest(__FILE__)) die ('No direct access allowed.');


class Engine {
		
		
	private static $dirs = array();
	private static $urls = array();
	
	private static $request = null;
	private static $segments = null;
	
	private function __construct() {}
	
	public static function dir($type, $suffix = '') {
		$val = @self::$dirs[$type];
		if ($val === null) return null;
		else return $val . $suffix;
	}
	
	public static function setDir($type, $dir) {
		Misc::uniformSlashes($dir);
		Misc::ensureSlashSuffix($dir);
		self::$dirs[$type] = $dir;
	}
	
	public static function url($type, $suffix = '') {
		$val = @self::$urls[$type];
		if ($val === null) return null;
		else return $val . $suffix;
	}
	
	public static function setURL($type, $url) {
		self::$urls[$type] = $url;
	}
	
	public static function getSegment($which) {
		if (count(self::$segments) > 0) {
			if (isset(self::$segments[$which])) return self::$segments[$which];
		}
		return null;
	}
	
	public static function getSegments() {
		return self::$segments;
	}
	
	public static function getRequest() {
		return self::$request;
	}
	
	private static function setDirIfNull($type, $value) {
		if (self::dir($type) === null) {
			self::setDir($type, $value);
		}
	}
	
	private static function setURLIfNull($type, $value) {
		if (self::url($type) === null) {
			self::setURL($type, $value);
		}
	}
	
	public static function setup() {
		
		self::setDirIfNull(DirType::ENGINE, dirname(__FILE__));
		self::setDirIfNull(DirType::SITE, dirname($_SERVER['SCRIPT_FILENAME']) . '/site');
		self::setDirIfNull(DirType::PUB, dirname($_SERVER['SCRIPT_FILENAME']) . '/pub');
		self::setDirIfNull(DirType::HANDLER, self::dir(DirType::SITE, 'handlers'));
		self::setDirIfNull(DirType::VIEW, self::dir(DirType::SITE, 'views'));
		self::setDirIfNull(DirType::LIB, self::dir(DirType::SITE, 'libs'));
		self::setDirIfNull(DirType::DB, self::dir(DirType::SITE, 'dbs'));
		
		if (self::url(URLType::BASE) === null) {
			throw new Exception('For Engine to work properly, base URL must be set');
		}
		
		self::setURLIfNull(URLType::INDEX, self::url(URLType::BASE, 'index.php/'));
		self::setURLIfNull(URLType::PUB, self::url(URLType::BASE, 'pub/'));
		
		self::$request = self::parsePathInfo();
		self::$segments = explode('/', self::$request);
	
		// remove last empty request array element (for example 'index.php/asd/' becomes 'index.php/asd')
		if (count(self::$segments) > 0) {
			if (end(self::$segments) === '') array_pop(self::$segments);
			reset(self::$segments);
		}
	}
	
	private static function parsePathInfo() {
		if (isset($_SERVER['PATH_INFO']) && strlen($_SERVER['PATH_INFO']) > 0) {
			return substr($_SERVER['PATH_INFO'], 1);
		} else {
			return '';
		}
	}
	
}
