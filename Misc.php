<?php
/** [LGPL] Copyright 2012 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Engine;

class Misc {
	
	/**
	 * Replace any amount of consecutive '\' or '/' with one '/'.
	 */
	public static function uniformSlashes(&$dir) {
		$dir = preg_replace('_[\\\\/]+_', '/', $dir);
	}
	
	/**
	 * Adds '/' to the end of the string if it's not there already.
	 */
	public static function ensureSlashSuffix(&$string) {
		if (substr($string, -1) !== '/') $string = $string . '/';
	}
	
	/**
	 * Check if HTTP client requested a file directly (__FILE__ could be fed as a parameter).
	 * @param string - $filepath - Path to file being checked for direct access.
	 * @return bool
	 */
	public static function isDirectRequest($filepath) {
		if (realpath($_SERVER['DOCUMENT_ROOT'] . $_SERVER['SCRIPT_NAME']) === realpath($filepath)) return true;
		else return false;
		
		/*$fullPath = str_replace('\\', '/', $currentFile);
		$fullRequestPath = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI']);
			
		if (strpos($fullRequestPath, $fullPath) === 0) return true;
		else return false;*/
	}
}
